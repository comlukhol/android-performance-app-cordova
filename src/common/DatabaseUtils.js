export function generateInsertIntoSampleEntity() {
    let sampleEntity = {
        stringContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vel malesuada urna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean varius tincidunt eleifend. Sed semper urna vehicula condimentum consequat. Proin in malesuada nisi. Nam et pretium arcu, ut rutrum felis. Nunc laoreet pharetra nisl luctus rutrum. Mauris ex odio, facilisis et ultricies vel, gravida eu ante. Phasellus bibendum ex eu pellentesque egestas. Etiam nec dictum sapien, eget molestie lectus. Aenean lorem leo, commodo ut sodales et, rutrum eu quam. Proin gravida, ex vitae tincidunt viverra, orci augue consectetur quam, nec dignissim nunc diam egestas lorem. Nullam rutrum cursus diam, eu pretium odio elementum eu. Nam quis consequat diam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed faucibus accumsan nibh, quis faucibus elit tempor sed. Maecenas id felis a lectus euismod volutpat. Sed gravida mollis tristique. Donec nec gravida urna. Nunc nec urna in odio volutpat accumsan. Suspendisse vel odio metus.",
        booleanContent: true,
        doubleContent: 1.551515515
    };

    let insertQuery = "INSERT INTO sample_entity(string_content, boolean_content, double_content) values('"+sampleEntity.stringContent+"', "+
                    (sampleEntity.booleanContent == true ? "1" : "0") + ", "+
                    sampleEntity.doubleContent+");"
    
    return insertQuery;
};

export function createSampleEntityTable() {
    return "CREATE TABLE IF NOT EXISTS sample_entity (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, string_content TEXT DEFAULT NULL, double_content REAL DEFAULT NULL, boolean_content INTEGER DEFAULT 0)";
}

export function getDropSampleEntityTableQuery() {
    return "DROP TABLE IF EXISTS sample_entity";
}

export function selectTableCount(db, tableName, successFun) {
    let query = "SELECT COUNT(*) AS rowsCount FROM " + tableName;
    db.executeSql(query, [], function(result) {
        successFun(result.rows.item(0).rowsCount);
    }, error => console.log(error));
};

export function selectAll(db, tableName, successFun) {
    let query = "SELECT * FROM " + tableName;
    db.executeSql(query, [], function(result) {
        successFun(result.rows);
    }, error => console.log(error));
};

export function insertAll(db, tableName, successFun) {
    const query = generateInsertIntoSampleEntity();
    const queryArray = [];
    for(let i = 0 ; i < 10000 ; i++) {
        queryArray.push(query);
    }

    db.sqlBatch(queryArray, function() {
        successFun();
    }, error => console.log(error));
};

export function dropTable(db, tableName, successFun) {
    const query = "DROP TABLE IF EXISTS " + tableName;
    db.executeSql(query, [], function(result) {
        successFun(result);
    }, error => console.log(error));
};

export function recreateSampleEntityTable(db, successFun) {
    const queries = [];
    queries.push(getDropSampleEntityTableQuery());
    queries.push(createSampleEntityTable());

    db.sqlBatch(queries, function() {
        successFun();
    }, error => console.log(error));
}