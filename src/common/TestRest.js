export function uploadTest(testDto, serverUrl, summaryId) {
    let extendedDto = Object.assign({}, testDto, {applicationType: "CORDOVA"});
    fetch(serverUrl+'/api/v1/test/save', {
        method: 'POST',
        body: JSON.stringify(extendedDto),
        headers:{
            'Content-Type': 'application/json'
        }
    });
}

export function uploadDomLoaded(elapsedMilis, serverUrl, summaryId) {
    fetch(serverUrl+'/api/v1/test/extraStartTime/save/'+summaryId+'/'+elapsedMilis, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    });
}