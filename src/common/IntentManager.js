import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import store from '../store';
import * as TestRest from './TestRest';

import ArithmeticPage from '../pages/ArithmeticPage';
import JsonSerializationPage from '../pages/JsonSerializationPage';
import JsonDeserializationPage from '../pages/JsonDeserializationPage';
import ReadFromDevicePage from '../pages/ReadFromDevicePage';
import SaveToDevicePage from '../pages/SaveToDevicePage';
import SaveToDatabasePage from '../pages/SaveToDatabasePage';
import ReadFromDatabasePage from '../pages/ReadFromDatabasePage';
import LoadLocationPage from '../pages/LoadLocationPage';
import RestQueriesPage from '../pages/RestQueriesPage'

import * as Actions from '../actions/Actions';
import * as SettingsAction from '../actions/SettingsAction';
import * as ArithmeticAction from '../actions/ArithmeticAction';
import * as JsonSerializationAction from '../actions/JsonSerializationAction';
import * as JsonDeserializationAction from '../actions/JsonDeserializationAction';
import * as ReadFromDeviceAction from '../actions/ReadFromDeviceAction';
import * as SaveToDeviceAction from '../actions/SaveToDeviceAction';
import * as SaveToDatabaseAction from '../actions/SaveToDatabaseAction';
import * as ReadFromDatabaseAction from '../actions/ReadFromDatabaseAction';
import * as LocationAction from '../actions/LocationAction';
import * as RestQueriesAction from '../actions/RestQueriesAction';

export function start() {
    window.plugins.intent.getCordovaIntent(function (Intent) {
        if(Intent.extras != undefined && Intent.extras.myExtra != undefined) {
            store.dispatch(SettingsAction.setTestIdAction(Intent.extras.myExtra));
            store.dispatch(SettingsAction.setServerUrl(Intent.extras.serverUrl));

            //First test:
            hashHistory.replace("/arithmeticPage");
            setTimeout(function() {
                DOMLoadTime = performance.now() - pageLoadStartTime;
                store.dispatch(ArithmeticAction.computeArithmetic());
                TestRest.uploadDomLoaded(DOMLoadTime, store.getState().settingsState.serverUrl, store.getState().settingsState.testId);
            }, 1000);
    
            var automatictTestsSubscription = store.subscribe(() => {
                let state = store.getState();

                switch(store.getState().lastAction.type) {

                    case Actions.ARITHMETIC_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'ARITHMETIC',
                            time: state.arithmeticState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/jsonSerializationPage"); }, 1000);
                        setTimeout(() => { store.dispatch(JsonSerializationAction.executeJsonSerialization()); }, 2000);
                        break;
                    }
                    case Actions.JSON_SERIALIZATION_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'JSON_SERIALIZATION',
                            time: state.jsonSerializationState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/jsonDeserializationPage"); }, 1000);
                        setTimeout(() => { store.dispatch(JsonDeserializationAction.executeJsonDeserialization()); }, 2000);
                        break;
                    }
    
                    case Actions.JSON_DESERIALIZATION_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'JSON_DESERIALIZATION',
                            time: state.jsonDeserializationState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/readFromDevicePage"); }, 1000);
                        setTimeout(() => { store.dispatch(ReadFromDeviceAction.readFromDevice()); }, 2000);
                        break;
                    }
    
                    case Actions.READ_FROM_DEVICE_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'READ_FROM_DEVICE',
                            time: state.readFromDeviceState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/saveToDevicePage"); }, 1000);
                        setTimeout(() => { store.dispatch(SaveToDeviceAction.saveToDevice()); }, 2000);
                        break;
                    }

                    case Actions.SAVE_TO_DEVICE_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'SAVE_TO_DEVICE',
                            time: state.saveToDeviceState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/saveToDatabasePage"); }, 1000);
                        setTimeout(() => { store.dispatch(SaveToDatabaseAction.saveToDatabase()); }, 2000);
                        break;
                    }

                    case Actions.SAVE_TO_DATABASE_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'SAVE_TO_DATABASE',
                            time: state.saveToDatabaseState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/restQueriesPage"); }, 1000);
                        setTimeout(() => { store.dispatch(RestQueriesAction.getWeather()); }, 2000);
                        break;
                    }

                    case Actions.REST_QUERIES_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'REST_QUERIES',
                            time: state.restQueriesState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/loadLocationPage"); }, 1000);
                        setTimeout(() => { store.dispatch(LocationAction.loadLocationAction()); }, 2000);
                        break;
                    }

                    case Actions.LOAD_LOCATION_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'READ_LAT_LNG',
                            time: state.loadLocationState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/readFromDatabasePage"); }, 1000);
                        setTimeout(() => { store.dispatch(ReadFromDatabaseAction.readFromDatabase()); }, 2000);
                        break;
                    }

                    case Actions.READ_FROM_DATABASE_END: {
                        setTimeout(() => {
                            TestRest.uploadTest({
                                testId: state.settingsState.testId,
                                testCode: 'LOAD_FROM_DATABASE',
                                time: state.readFromDatabaseState.executionTime 
                            }, state.settingsState.serverUrl);
                        }, 2000);
                        setTimeout(() => { console.log('performance-tests-ended'); }, 4000);
                        automatictTestsSubscription();
                    }
                }
            });
        }
    }, function () {
        console.log('Error at intent.');
    });
}

/*case Actions.ARITHMETIC_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'ARITHMETIC',
                            time: state.arithmeticState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/jsonSerializationPage"); }, 1000);
                        setTimeout(() => { store.dispatch(JsonSerializationAction.executeJsonSerialization()); }, 2000);
                        break;
                    }
                    case Actions.JSON_SERIALIZATION_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'JSON_SERIALIZATION',
                            time: state.jsonSerializationState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/jsonDeserializationPage"); }, 1000);
                        setTimeout(() => { store.dispatch(JsonDeserializationAction.executeJsonDeserialization()); }, 2000);
                        break;
                    }
    
                    case Actions.JSON_DESERIALIZATION_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'JSON_DESERIALIZATION',
                            time: state.jsonDeserializationState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/readFromDevicePage"); }, 1000);
                        setTimeout(() => { store.dispatch(ReadFromDeviceAction.readFromDevice()); }, 2000);
                        break;
                    }
    
                    case Actions.READ_FROM_DEVICE_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'READ_FROM_DEVICE',
                            time: state.readFromDeviceState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/saveToDevicePage"); }, 1000);
                        setTimeout(() => { store.dispatch(SaveToDeviceAction.saveToDevice()); }, 2000);
                        break;
                    }

                    case Actions.SAVE_TO_DEVICE_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'SAVE_TO_DEVICE',
                            time: state.saveToDeviceState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/readFromDatabasePage"); }, 1000);
                        setTimeout(() => { store.dispatch(ReadFromDatabaseAction.readFromDatabase()); }, 2000);
                        break;
                    }

                    case Actions.READ_FROM_DATABASE_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'LOAD_FROM_DATABASE',
                            time: state.readFromDatabaseState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/saveToDatabasePage"); }, 1000);
                        setTimeout(() => { store.dispatch(SaveToDatabaseAction.saveToDatabase()); }, 2000);
                        break;
                    }

                    case Actions.SAVE_TO_DATABASE_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'SAVE_TO_DATABASE',
                            time: state.saveToDatabaseState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/loadLocationPage"); }, 1000);
                        setTimeout(() => { store.dispatch(LocationAction.loadLocationAction()); }, 2000);
                        break;
                    }

                    case Actions.LOAD_LOCATION_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'READ_LAT_LNG',
                            time: state.loadLocationState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { hashHistory.replace("/restQueriesPage"); }, 1000);
                        setTimeout(() => { store.dispatch(RestQueriesAction.getWeather()); }, 2000);
                        break;
                    }

                    case Actions.REST_QUERIES_END: {
                        TestRest.uploadTest({
                            testId: state.settingsState.testId,
                            testCode: 'REST_QUERIES',
                            time: state.restQueriesState.executionTime 
                        }, state.settingsState.serverUrl);
                        setTimeout(() => { console.log('performance-tests-ended'); }, 1000);
                        automatictTestsSubscription();
                    }*/