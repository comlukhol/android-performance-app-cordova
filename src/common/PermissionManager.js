export function checkPermission() {
    try {
        let permissions = cordova.plugins.permissions;

        permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function(status){
            if (status.hasPermission) {
                
            }
            else {
                permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE, ()=>{}, ()=>{});
            }
        });

        permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE, function(status) {
            if (!status.hasPermission) {
                permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, ()=>{}, ()=>{});
            }
        });
    } catch(e) {
        console.log(e);
    }
}

//https://www.npmjs.com/package/cordova-plugin-android-permissions