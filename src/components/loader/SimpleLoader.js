import React from 'react';
import './SimpleLoader.css';

export default class SimpleLoader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: this.props.message
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            message: nextProps.message,
        });
    }

    render() {
        return (
            <div class="s-full-page-loader">
                <div class="s-loader-message">
                    {this.state.message}
                </div>
                <div class="s-loader">
                    <div class="s-loader-inner">
                    </div>
                </div>
            </div>  
        )
    }
}

SimpleLoader.defaultProps = {
    message: "Loading..."
}