import React from 'react';
import { connect } from 'react-redux';

@connect((store) => {
    return {
        menuStore: store.menuState
    }
})
export default class StartPage extends React.Component {
    constructor() {
        super();
    }

    render() {
        const { menuStore } = this.props;

        return (
            <div className="upper-section-container">
                <span>
                    {menuStore.menuDescription}
                </span>
                <hr/>
            </div>
        )
    }
}