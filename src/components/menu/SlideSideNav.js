import React from 'react';
import './Toolbar.css';

export default class SlideSideNav extends React.Component {
    constructor(props) {
        super(props);

        this.maxOpacity = 0.5;

        this.globalStyle = {
            height: this.props.hamburgerSize,
            width: this.props.hamburgerSize,
            margin: "0",
            float: "left",
            padding: "20px"
        };

        this.hamburgerLineStyle = {
            width: "18px",
            height: "2px",
            backgroundColor: this.props.hamburgerColor,
            margin: "3px 0"
        };

        this.state = {
            tabStyle: {
                left: "-80%",
                color: this.props.slideNavStyle.color + "!important",
                backgroundColor: this.props.slideNavStyle.backgroundColor + "!important",
                fontSize: this.props.slideNavStyle.fontSize
            },
            maskStyle: {
                opacity: 0
            },
            menuItems: this.props.menuItems
        };

        this.showMenu = this.showMenu.bind(this);
        this.hideMenu = this.hideMenu.bind(this);
        this.onMenuClicked = this.onMenuClicked.bind(this);
        this.onSlideTabMove = this.onSlideTabMove.bind(this);
        this.onSideTabMouseDown = this.onSideTabMouseDown.bind(this);
        this.onSideTabMouseUp = this.onSideTabMouseUp.bind(this);

        this.slideSideTab = {
            isGrabbed: false,
            mouseDownX: 0,
            mouseUpX: 0,
            lastX: 0,
            screenX: 0,
            velocity: 0
        };

        document.onclick = e => {
            if(e.target.id != 'side-nav-tab' && e.target.id != 'menu-hamburger' && !e.target.classList.contains("hamburger-line"))
                this.hideMenu();
        };

        document.ontouchend = e => {
            this.slideSideTab.isGrabbed = false;
        };

        document.onmouseup = e => {
            this.slideSideTab.isGrabbed = false;
        };
    }

    onMenuClicked() {
        this.showMenu();
        if(this.props.onMenuClicked != undefined)
            this.props.onMenuClicked();
    }

    showMenu() {
        this.setState({
            tabStyle: Object.assign({}, this.state.tabStyle, {
                left: "0%",  
                transition: "0.3s"              
            }), 
            maskStyle: {
                opacity: this.maxOpacity,
                transition: "0.3s"
            }
        });
    }

    hideMenu() {
        this.setState({
            tabStyle: Object.assign({}, this.state.tabStyle, {
                left: "-80%",  
                transition: "0.3s"              
            }),
            maskStyle: {
                opacity: 0,
                transition: "0.3s"
            }
        });
    }

    onSlideTabMove(e) {
        if(this.slideSideTab.isGrabbed) {
            let visibleScreenWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth || 0;

            let screenX;
            if(e.screenX != undefined) {
                screenX = e.screenX;
            } else {
                screenX = e.changedTouches[0].screenX;
            }

            let newTabSizePercentageSize = ((this.slideSideTab.lastX - screenX) / visibleScreenWidth) * 100;
            
            let actualMenuPosition = this.state.tabStyle.left.slice(0, -1);
            let positionToSet = (this.state.tabStyle.left.slice(0, -1) - newTabSizePercentageSize);
        

            if(positionToSet > 0.0) {
                positionToSet = 0;
            }

            let maskOpacityToSet = this.maxOpacity + (positionToSet / 100);
            if(this.state.maskStyle.opacity > this.maxOpacity) {
                maskOpacityToSet = this.maxOpacity;
            }
            
            this.setState({
                tabStyle: Object.assign({}, this.state.tabStyle, {
                    left: positionToSet + "%",
                    transition: "none"
                }),
                maskStyle: {
                    opacity: maskOpacityToSet,
                    transition: "none"
                }
            });

            this.slideSideTab.velocity = this.slideSideTab.lastX - screenX;
            this.slideSideTab.lastX = screenX;

            if(this.slideSideTab.velocity < -20) {
                this.onMenuClicked();
                this.slideSideTab.isGrabbed = false;
            }

            if(this.slideSideTab.velocity > 20) {
                this.hideMenu();
                this.slideSideTab.isGrabbed = false;
            }
        }
    }

    onSideTabMouseDown(e) {
        this.slideSideTab.isGrabbed = true;
        if(e.screenX != undefined) {
            this.slideSideTab.mouseDownX = e.screenX;
            this.slideSideTab.lastX = e.screenX;
        } else {
            this.slideSideTab.mouseDownX = e.changedTouches[0].screenX
            this.slideSideTab.lastX = e.changedTouches[0].screenX
        }
    }

    onSideTabMouseUp(e) {
        this.slideSideTab.isGrabbed = true;
        if(this.state.tabStyle.left.slice(0, -1) > -40) {
            this.showMenu();
        } else {
            this.hideMenu();
        }
    }

    componentWillReceiveProps(nextProps) {
        // this.setState({
        //     menuItems: nextProps.menuItems,
        //     tabStyle: Object.assign({}, this.state.tabStyle, {
        //         color: nextProps.slideNavStyle.color + "!important",
        //         backgroundColor: nextProps.slideNavStyle.backgroundColor + "!important",

        //     })
        // });
        this.setState(nextProps);
    }

    render() {
        let menuItems = "";

        if(this.state.menuItems != null) {
            menuItems = [];
            for(let i = 0 ; i < this.state.menuItems.length ; i++) {
                let tmpItem = this.state.menuItems[i];

                if(tmpItem.separator != undefined && tmpItem.separator) {
                    menuItems.push(
                        <span key={i}>
                            <hr style={{backgroundColor: this.state.tabStyle.color}}/>
                            {tmpItem.title}
                        </span>
                    );
                } else {     
                    menuItems.push(
                        <div className="menu-item" onClick={tmpItem.onClick} key={i}>
                            {tmpItem.icon == null ? "" : <img width="50" height="50" src={tmpItem.icon} style={{padding: "10px"}}/>}
                            <span style={{paddingLeft: "8px"}}>
                                {tmpItem.title}
                            </span>
                        </div>
                    );
                }
            }
        }

        return (
            <div>
                <div id="menu-hamburger" style={this.globalStyle} onClick={this.onMenuClicked} >
                    <div className="hamburger-line"></div>
                    <div className="hamburger-line"></div>
                    <div className="hamburger-line"></div>
                </div>
                <div className="tmla-mask" style={this.state.maskStyle}>
                </div>
                <div id="side-nav-tab"
                        style={this.state.tabStyle}
                        className="sidenav" 
                        onMouseMove={this.onSlideTabMove} 
                        onMouseDown={this.onSideTabMouseDown}
                        onMouseUp={this.onSideTabMouseUp} 
                        onTouchMove={this.onSlideTabMove}
                        onTouchStart={this.onSideTabMouseDown }
                        onTouchEnd={this.onSideTabMouseUp}
                >
                    {this.props.upperSection}
                    <div style={{padding: "8px"}}>
                        {menuItems}
                    </div>
                </div>
                <div className="slide-nav-handler"
                    onMouseMove={this.onSlideTabMove} 
                    onMouseDown={this.onSideTabMouseDown}
                    onMouseUp={this.onSideTabMouseUp} 
                    onTouchMove={this.onSlideTabMove}
                    onTouchStart={this.onSideTabMouseDown }
                    onTouchEnd={this.onSideTabMouseUp}>
                </div>
            </div>
        )
    }
}

SlideSideNav.defaultProps = {
    onMenuClicked: function() {},
    hamburgerSize: "55px",
    hamburgerColor: "white",
    menuItems: null,
    upperSection: "",
    slideNavStyle: {
        color: "black",
        backgroundColor: "white"
    }
}