import React from 'react';
import SlideSideNav from './SlideSideNav';
import Center from './Center';
import Submenu from './Submenu';

export default class Toolbar extends React.Component {
    constructor(props) {
        super(props);

        this.globalStyle = {
            height: this.props.style.height,
            color: this.props.style.color,
            width: "100%",
            margin: "0",
            backgroundColor: this.props.style.backgroundColor,
            boxShadow: "0px -2px 20px 1px #888888",
            zIndex: "10000"
        };

        this.centerStyle = {
            width: (this.props.submenu ? "calc(100% - 110px)" : "calc(100% - 55px)"),
            height: this.props.height,
            fontAttribute: "bold",
            fontSize: "20px",
            color: "white"
        };

        this.state = {
            menuItems: this.props.menuItems,
            submenuItems: this.props.submenuItems,
            title: this.props.title,
            icon: this.props.icon,
            logo: this.props.logo,
            slideNavStyle: this.props.slideNavStyle
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            menuItems: nextProps.menuItems,
            submenuItems: this.props.submenuItems,
            title: nextProps.title,
            icon: this.props.icon,
            logo: this.props.logo,
            slideNavStyle: this.props.slideNavStyle
        });
    }

    render() {
        return (
            <div style={this.globalStyle}>
                <SlideSideNav hamburgerSize={this.globalStyle.height}
                              onMenuClicked={this.props.methods.onMenuClicked} 
                              onSubmenuClicked={this.props.methods.onSubmenuClicked} 
                              menuItems={this.state.menuItems}
                              upperSection={this.props.upperSection}
                              slideNavStyle={this.state.slideNavStyle}/>
                <Center style={this.centerStyle}
                        title={this.state.title}
                        logo={this.state.logo}
                        icon={this.state.icon}
                        onClick={this.props.methods.onCenterClicked} />
                {this.props.submenu ? <Submenu/> : ""}
            </div>
        )
    }
}

Toolbar.defaultProps = {
    style: {
        height: "55px",
        backgroundColor: "#3F51B5",
        color: "white",
    },
    slideNavStyle: {
        backgroundColor: "white",
        color: "black",
        fontSize: "20px"
    },
    options: {
        slideNavUpperSection: true,
        displaySubmenu: false
    },
    menuItems: null,
    submenuItems: null,
    title: "",
    upperSection: "",
    icon: null,
    logo: null,
    methods: {
        onMenuClicked: null,
        onCenterClicked: null,
        onSubmenuClicked: null
    }
};