import React from 'react';

export default class Center extends React.Component {
    constructor(props) {
        super(props);

        this.innerOnClick = this.innerOnClick.bind(this);

        this.style = Object.assign({}, this.props.style, {
            float: "left",
            padding: "10px",
            paddingTop: "15px"
        });

        this.state = {
            title: this.props.title,
            logo: this.props.logo,
            icon: this.props.icon
        };
    }

    innerOnClick() {


        if(this.props.onClick != null) {
            this.props.onClick();
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            title: nextProps.title,
            logo: nextProps.logo,
            icon: nextProps.icon
        });
    }

    render() {
        return (
            <div style={this.style} onClick={this.innerOnClick}>
                <div className="text-center-height">
                    {this.state.title}
                </div>
            </div>
        )
    }
}

Center.defaultProps = {
    title: "",
    logo: null,
    icon: null
}