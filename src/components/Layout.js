import React from 'react';
import { connect } from 'react-redux';
import Toolbar from './menu/Toolbar';
import UpperSection from './UpperSection';

@connect((store) => {
    return {
        menuStore: store.menuState
    };
})
export default class Layout extends React.Component {
    render() {
        const { menuStore } = this.props;

        return (
            <div>
                 <Toolbar title={menuStore.title}
                          menuItems={menuStore.menuItems} 
                          upperSection={<UpperSection/>}/>
                <div className="container">
                    {this.props.children}
                </div>
            </div>
        )
    }
}