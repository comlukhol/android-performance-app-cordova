import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import SimpleLoader from '../components/loader/SimpleLoader';

export default class TestContent extends React.Component {
    constructor(props) {
        super(props);
        this.onButtonClciked = this.onButtonClciked.bind(this);
        this.state = {
            result: this.props.result,
            executionTime: this.props.executionTime,
            isBusy: this.props.isBusy,
            testDescription: this.props.testDescription,
            loaderMessage: this.props.loaderMessage
        };

        this.counter = 0;
    }

    onButtonClciked() {
        if(this.state.isBusy)
            return;

        this.props.onButtonClicked();
        this.counter++;
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            result: nextProps.result,
            executionTime: nextProps.executionTime,
            isBusy: nextProps.isBusy,
            testDescription: nextProps.testDescription,
            loaderMessage: nextProps.loaderMessage
        });
    }

    render() {
        let loaderSection = "";
        if(this.state.isBusy) {
            loaderSection = (<SimpleLoader message={this.state.testDescription} />);
        }

        return (
            <div className="slide-in">
                {loaderSection}
                <div className="container">
                    <div class="row">
                        <div class="col-xs-12" style={{margin: "10px"}}>
                            {this.state.testDescription}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div className="text-center display-middle">
                                <div>
                                    <span>
                                        Elapsed time: {this.state.executionTime}
                                    </span>
                                    <br/>
                                    <div style={{wordBreak: "break-all", maxHeight: "200px!imporant", height: "200px!important", overflowX: "auto", width: "100%"}}>
                                        Result: {this.state.result}
                                    </div>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div className="btn btn-primary btn-block btn-bottom"
                                 onClick={this.onButtonClciked}>
                                Start
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}