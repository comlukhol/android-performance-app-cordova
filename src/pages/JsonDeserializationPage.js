import React from 'react';
import { connect } from 'react-redux';
import TestContent from '../components/TestContent';
import store from '../store';

import * as JsonDeserializationAction from '../actions/JsonDeserializationAction';

class JsonDeserializationPage extends React.Component {
    constructor(props) {
        super(props);
        this.onJsonDeserializationButtonClicked = this.onJsonDeserializationButtonClicked.bind(this);
    }

    onJsonDeserializationButtonClicked() {
        store.dispatch(JsonDeserializationAction.executeJsonDeserialization());
    }

    render() {
        const { jsonDeserializationState } = this.props;

        return (
            <TestContent result={jsonDeserializationState.result}
                         executionTime={jsonDeserializationState.executionTime}
                         isBusy={jsonDeserializationState.isBusy}
                         testDescription={"Test deserializacji jsonów w postaci stringa do obiektu. Ten sam string jest deserializowany 10 000 razy."}
                         onButtonClicked={this.onJsonDeserializationButtonClicked} />
        )
    }
}

const mapStateToProps = store => {
    return {
        jsonDeserializationState: store.jsonDeserializationState
    }
};

const mapDispatchToProps = dispatch => {
    return { };
};

export default connect(mapStateToProps, mapDispatchToProps)(JsonDeserializationPage);