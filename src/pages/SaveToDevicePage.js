import React from 'react';
import { connect } from 'react-redux';
import TestContent from '../components/TestContent';

import * as SaveToDeviceAction from '../actions/SaveToDeviceAction';

class SaveToDevicePage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { saveToDeviceState } = this.props;
        const { saveToDevice } = this.props;

        return (
            <TestContent result={saveToDeviceState.result}
                         executionTime={saveToDeviceState.executionTime}
                         isBusy={saveToDeviceState.isBusy}
                         testDescription={"Test zapisywania plików na urządzeniu. 100 iteracji."}
                         onButtonClicked={saveToDevice} />
        )
    }
}

const mapStateToProps = store => {
    return {
        saveToDeviceState: store.saveToDeviceState
    }
};

const mapDispatchToProps = dispatch => {
    return { 
        saveToDevice: () => {
            dispatch(SaveToDeviceAction.saveToDevice())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SaveToDevicePage);