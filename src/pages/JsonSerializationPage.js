import React from 'react';
import { connect } from 'react-redux';
import TestContent from '../components/TestContent';

import * as JsonSerializationAction from '../actions/JsonSerializationAction';

@connect((store) => {
    return {
        jsonSerializationState: store.jsonSerializationState
    };
})
export default class JsonSerializationPage extends React.Component {
    constructor() {
        super();
        this.onJsonSerializationButtonClicked = this.onJsonSerializationButtonClicked.bind(this);
    }

    onJsonSerializationButtonClicked() {
        this.props.dispatch(JsonSerializationAction.executeJsonSerialization());
    }

    render() {
        const { jsonSerializationState } = this.props;

        return (
            <TestContent result={jsonSerializationState.result}
                         executionTime={jsonSerializationState.executionTime}
                         isBusy={jsonSerializationState.isBusy}
                         testDescription={"Test serializacji obiektów do Stringa w postaci Jsona. Ten sam obiekt jest serializowany 10 000 razy."}
                         onButtonClicked={this.onJsonSerializationButtonClicked} />
        )
    }
}