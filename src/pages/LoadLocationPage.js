import React from 'react';
import { connect } from 'react-redux';
import TestContent from '../components/TestContent';
import store from '../store';

import * as LocationAction from '../actions/LocationAction';

class LoadLocationPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { loadLocationState } = this.props;
        const { loadLocation } = this.props;

        return (
            <TestContent result={loadLocationState.result}
                         executionTime={loadLocationState.executionTime}
                         isBusy={loadLocationState.isBusy}
                         testDescription={"Lokalizacja urządzenia"}
                         onButtonClicked={loadLocation} />
        )
    }
}

const mapStateToProps = store => {
    return {
        loadLocationState: store.loadLocationState
    }
};

const mapDispatchToProps = dispatch => {
    return { 
        loadLocation: () => {
            dispatch(LocationAction.loadLocationAction());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoadLocationPage);