import React from 'react';
import { connect } from 'react-redux';
import TestContent from '../components/TestContent';
import store from '../store';

import * as ReadFromDeviceAction from '../actions/ReadFromDeviceAction';

class ReadFromDevicePage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { readFromDeviceState } = this.props;
        const { readFromDevice } = this.props;

        return (
            <TestContent result={readFromDeviceState.result}
                         executionTime={readFromDeviceState.executionTime}
                         isBusy={readFromDeviceState.isBusy}
                         testDescription={"Test ładowania plików z urządzenia. 100 iteracji."}
                         onButtonClicked={readFromDevice} />
        )
    }
}

const mapStateToProps = store => {
    return {
        readFromDeviceState: store.readFromDeviceState
    }
};

const mapDispatchToProps = dispatch => {
    return { 
        readFromDevice: () => {
            dispatch(ReadFromDeviceAction.readFromDevice())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReadFromDevicePage);