import React from 'react';
import { connect } from 'react-redux';
import TestContent from '../components/TestContent';
import store from '../store';

import * as RestQueriesAction from '../actions/RestQueriesAction';

class RestQueriesPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { restQueriesState } = this.props;
        const { restQueries } = this.props;

        return (
            <TestContent result={restQueriesState.result}
                         executionTime={restQueriesState.executionTime}
                         isBusy={restQueriesState.isBusy}
                         testDescription={"Test zapytań REST."}
                         onButtonClicked={restQueries} />
        )
    }
}

const mapStateToProps = store => {
    return {
        restQueriesState: store.restQueriesState
    }
};

const mapDispatchToProps = dispatch => {
    return { 
        restQueries: () => {
            dispatch(RestQueriesAction.getWeather())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RestQueriesPage);