import React from 'react';
import { connect } from 'react-redux';

import * as ArithmeticAction from '../actions/ArithmeticAction';
import TestContent from '../components/TestContent';

class AirhtmeticPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { arithmeticState, computeArithmetic } = this.props;

        return (
            <TestContent result={arithmeticState.result}
                         executionTime={arithmeticState.executionTime}
                         isBusy={arithmeticState.isBusy}
                         testDescription={"Test obliczający liczbę PI z 10000 liczbami po przecinku. Kolejno generowane są liczby po przecinku, a następnie łączone w jeden napis (String). Test ten uwzględnia standardowe operacje matematyczne: dodawanie, odejmownaie, dzielenie, mnożenie oraz modulo. Dodatkowo wykorzystywane są tablice oraz operacje konkatenacji napisów."}
                         onButtonClicked={computeArithmetic} />
        )
    }
}

const mapStateToProps = store => {
    return {
        arithmeticState: store.arithmeticState
    }
};

const mapDispatchToProps = dispatch => {
    return {
        computeArithmetic: () => {
            dispatch(ArithmeticAction.computeArithmetic());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AirhtmeticPage);