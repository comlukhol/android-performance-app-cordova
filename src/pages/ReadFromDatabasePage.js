import React from 'react';
import { connect } from 'react-redux';
import TestContent from '../components/TestContent';

import * as ReadFromDatabaseAction from '../actions/ReadFromDatabaseAction';

class ReadFromDatabasePage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { readFromDatabaseState } = this.props;
        const { readFromDatabase } = this.props;

        return (
            <TestContent result={readFromDatabaseState.result}
                         executionTime={readFromDatabaseState.executionTime}
                         isBusy={readFromDatabaseState.isBusy}
                         testDescription={"Test odczytywania z bazy danych."}
                         onButtonClicked={readFromDatabase} />
        )
    }
}

const mapStateToProps = store => {
    return {
        readFromDatabaseState: store.readFromDatabaseState
    }
};

const mapDispatchToProps = dispatch => {
    return {
        readFromDatabase: () => {
            dispatch(ReadFromDatabaseAction.readFromDatabase());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReadFromDatabasePage);