import React from 'react';
import { connect } from 'react-redux';
import TestContent from '../components/TestContent';

import * as SaveToDatabaseAction from '../actions/SaveToDatabaseAction';

class SaveToDatabasePage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { saveToDatabaseState } = this.props;
        const { saveToDatabase } = this.props;

        return (
            <TestContent result={saveToDatabaseState.result}
                         executionTime={saveToDatabaseState.executionTime}
                         isBusy={saveToDatabaseState.isBusy}
                         testDescription={"Test zapisywania do bazy danych."}
                         onButtonClicked={saveToDatabase} />
        )
    }
}

const mapStateToProps = store => {
    return {
        saveToDatabaseState: store.saveToDatabaseState
    }
};

const mapDispatchToProps = dispatch => {
    return {
        saveToDatabase: () => {
            dispatch(SaveToDatabaseAction.saveToDatabase());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SaveToDatabasePage);