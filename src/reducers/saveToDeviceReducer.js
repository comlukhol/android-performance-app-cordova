import * as Actions from '../actions/Actions';

export default function reduce(state = {
	result: 0,
	executionTime: 0,
	isBusy: false
}, action) {
	switch(action.type) {
		case Actions.SAVE_TO_DEVICE_START: {
			return {
				...state,
				isBusy: true,
				result: 0,
				executionTime: 0
			}
		}

		case Actions.SAVE_TO_DEVICE_END: {
			return {
				...state,
				isBusy: false,
				result: action.payload.result,
				executionTime: action.payload.executionTime
			}
		}
	}

	return state;
}