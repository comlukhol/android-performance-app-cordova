import { combineReducers } from 'redux';

import arithmeticState from './arithmeticReducer';
import menuState from './menuReducer';
import settingsState from './settingsReducer';
import jsonSerializationState from './jsonSerializationReducer';
import jsonDeserializationState from './jsonDeserializationReducer';
import readFromDeviceState from './readFromDeviceReducer';
import saveToDeviceState from './saveToDeviceReducer';
import saveToDatabaseState from './saveToDatabaseReducer';
import readFromDatabaseState from './readFromDatabaseReducer';
import loadLocationState from './locationReducer';
import restQueriesState from './restQueriesReducer';

function lastAction(state = null, action) {
	return action;
}

export default combineReducers({
	arithmeticState,
	menuState,
	settingsState,
	jsonSerializationState,
	jsonDeserializationState,
	readFromDeviceState,
	saveToDeviceState,
	saveToDatabaseState,
	readFromDatabaseState,
	loadLocationState,
	restQueriesState,
	lastAction
});