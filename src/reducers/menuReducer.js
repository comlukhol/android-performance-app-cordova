import { hashHistory } from "react-router";

export default function reduce(state = {
    menuItems: [
        {
            title: "Arithmetic",
            onClick: () => { hashHistory.replace("/arithmeticPage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        },
        {
            title: "Json serialization",
            onClick: () => { hashHistory.replace("/jsonSerializationPage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        },
        {
            title: "Json deserialization",
            onClick: () => { hashHistory.replace("/jsonDeserializationPage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        },
        {
            title: "Read from device",
            onClick: () => { hashHistory.replace("/readFromDevicePage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        },
        {
            title: "Save to device",
            onClick: () => { hashHistory.replace("/saveToDevicePage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        },
        {
            title: "Save to database",
            onClick: () => { hashHistory.replace("/saveToDatabasePage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        },
        {
            title: "Read from database",
            onClick: () => { hashHistory.replace("/readFromDatabasePage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        },
        {
            title: "Load device location",
            onClick: () => { hashHistory.replace("/loadLocationPage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        },
        {
            title: "Rest queries",
            onClick: () => { hashHistory.replace("/restQueriesPage"); },
            icon: "https://i.stack.imgur.com/fcqjS.png"
        }
    ],
    title: "Cordova Application",
    menuDescription: "Aplikacja, której celem jest sprawdzenie wydajności niektóry operacji. Na liście poniżej znajdują się poszczególne operacje, które można wybrać.",
    slideNavStyle: {
        backgroundColor: "#232323",
        color: "white",
        fontSize: "16px"
    }
}, action) {
    return state;
}