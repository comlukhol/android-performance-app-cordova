import * as Actions from '../actions/Actions';

export default function reduce(state = {
	testId: null,
	serverUrl: null
}, action) {
    switch(action.type) {
		case Actions.SET_TEST_ID: {
			return {
				...state,
				testId: action.payload.testId,
			}
		}

		case Actions.SET_SERVER_URL: {
			return {
				...state,
				serverUrl: action.payload.serverUrl
			}
		}
	}

	return state;
}