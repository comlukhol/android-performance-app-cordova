import * as Actions from './Actions';

export function computeArithmetic() {
    return function(dispatch) {
        dispatch({type: Actions.ARITHMETIC_START});

        var calulcatePIFun = function(digits) {
            const SCALE = 10000;
            const ARRINIT = 2000;

            let pi = [];
            let arr = [];
            let carry = 0;

            for(let i = 0; i <= digits ; ++i) {
                arr[i] = ARRINIT;
            }

            for(let i = digits ; i>0 ; i-= 14) {
                let sum = 0;
                for (let j = i; j > 0; --j) {
                    sum = sum * j + SCALE * arr[j];
                    arr[j] = sum % (j * 2 - 1);
                    sum /= j * 2 - 1;
                }

                //console.log(carry + sum/SCALE);
                pi.push(Math.floor(carry + sum/SCALE));
                carry = sum % SCALE;
            }

            console.log(pi.length * 4);
            return pi.join("");
        };

        setTimeout(function() {
            const promise = new Promise(function(resolve, reject) {
                let t0 = performance.now();
                let sum = calulcatePIFun(35000);
    
                sum = sum.substr(1);
                sum = '3.' + sum;

                let t1 = performance.now();
                let timeResult = t1-t0;
                let operationResult = sum;
                
                resolve({
                    result: operationResult,
                    executionTime: timeResult
                });
            });
            
            promise.then(function(res) {
                dispatch({
                    type: Actions.ARITHMETIC_END,
                    payload: {
                        result: res.result,
                        executionTime: res.executionTime
                    }
                });
            });
        }, 1);
    }
}