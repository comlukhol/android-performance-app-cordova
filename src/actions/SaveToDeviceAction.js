import * as Actions from './Actions';

export function saveToDevice() {
    return function(dispatch) {
        dispatch({type: Actions.SAVE_TO_DEVICE_START});

        setTimeout(function() {
            const oryginalImageLocation = "file:///sdcard/performanceTests/photos/android-photo-small.jpg";
            const directoryForImagesLocation = "file:///sdcard/performanceTests/photos/write";
            const myRootDirectory = "file:///sdcard/performanceTests/photos"

            var t0 = performance.now();
            var imageArrayBuffer;
            var loadFileFun;
            var count = 0;
            
            const saveFileFun = () => {
                window.resolveLocalFileSystemURL(directoryForImagesLocation, fileEntry => {
                    let fileName = "android-photo-small-" + count + ".jpg";
                    fileEntry.getFile(fileName, {create: true, exclusive: false}, fileEntry => {
                        fileEntry.createWriter(fileWriter => {
                            fileWriter.write(imageArrayBuffer);
                            fileWriter.onwriteend = () => {
                                if(count++ < 100) {
                                    saveFileFun();
                                } else {
                                    let t1 = performance.now();
                                    let timeResult = t1-t0;

                                    dispatch({
                                        type: Actions.SAVE_TO_DEVICE_END,
                                        payload: {
                                            result: "100 times saved",
                                            executionTime: timeResult
                                    }});
                                }
                            };

                            fileWriter.onerror = (error) => {
                                console.log(error);
                            };
                        }, error => console.log(error));
                    }, error => console.log(error));
                }, error => console.log(error));
            };

            const createDirectoryFun = () => {
                window.resolveLocalFileSystemURL(myRootDirectory, fileEntry => {
                    fileEntry.getDirectory('write', { create: true }, createdDirectory => {
                        loadFileFun();
                    });
                }, error => {
                    loadFileFun();
                });
            };

            const checkIfDirectoryExistFun = () => {
                window.resolveLocalFileSystemURL(directoryForImagesLocation, fileEntry => {
                    if(fileEntry.isDirectory) {
                        fileEntry.removeRecursively(success => {
                            createDirectoryFun();
                        }, error => console.log(error));
                    } 
                }, error => {
                    createDirectoryFun();
                });
            };

            loadFileFun = () => {
                window.resolveLocalFileSystemURL(oryginalImageLocation, fileEntry => {
                    fileEntry.file(readyFile => {
                        const reader = new FileReader();
                        reader.onload = function(evt) {
                            imageArrayBuffer = evt.target.result;
                            saveFileFun();
                        }
                        
                        reader.readAsArrayBuffer(readyFile); 
                    });
                }, error => console.log(error));
            };

            checkIfDirectoryExistFun();
        }, 1);
    }
}

