import * as Actions from './Actions';

export function readFromDevice() {
    return function(dispatch) {
        dispatch({type: Actions.READ_FROM_DEVICE_START});

        setTimeout(function() {
            var t0 = performance.now();
            var loadCout = 0;

            //test code here
            const location = "file:///sdcard/performanceTests/photos/android-photo-small.jpg";
            let base64Image;

            const loadFileFun = () => {
                window.resolveLocalFileSystemURL(location, fileEntry => {
                    fileEntry.file(readyFile => {
                        const reader = new FileReader();
                        reader.onload = function(evt) {
                            if(loadCout++ < 100) {
                                loadFileFun();
                            } else {
                                let t1 = performance.now();
                                let timeResult = t1-t0;
                                
                                dispatch({
                                    type: Actions.READ_FROM_DEVICE_END,
                                    payload: {
                                        result: "100 times loaded",
                                        executionTime: timeResult
                                    }
                                }); 
                            }
                        }
                        reader.readAsArrayBuffer(readyFile); 
                    });
                }, error => console.log(error));
            };

            loadFileFun();
        }, 1);
    }
}