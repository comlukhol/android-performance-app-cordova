import * as DatabaseUtils from '../common/DatabaseUtils';
import * as Actions from './Actions';

export function saveToDatabase() {
    return function(dispatch) {
        dispatch({type: Actions.SAVE_TO_DATABASE_START});

        var t0;
        var db = window.sqlitePlugin.openDatabase({name: 'android-performance-tests.db', location: 'default'});

        DatabaseUtils.recreateSampleEntityTable(db, () => {
            t0 = performance.now();
            DatabaseUtils.insertAll(db, "sample_entity", result => {
                const t1 = performance.now();
                const timeResult = t1-t0;
                DatabaseUtils.selectTableCount(db, "sample_entity", rowsCount => {
                    DatabaseUtils.dropTable(db, "sample_entity", result => {
                        dispatch({
                            type: Actions.SAVE_TO_DATABASE_END,
                            payload: {
                                result: rowsCount,
                                executionTime: timeResult
                            }
                        });
                    });
                });
            });
        });
    }
};