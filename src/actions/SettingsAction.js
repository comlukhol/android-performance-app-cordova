import * as Actions from './Actions';

export function setTestIdAction(testId) {
    return {
        type: Actions.SET_TEST_ID,
        payload: {
            testId: testId
        }
    }
}

export function setServerUrl(serverUrl) {
    return {
        type: Actions.SET_SERVER_URL,
        payload: {
            serverUrl
        }
    }
}