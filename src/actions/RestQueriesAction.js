import * as Actions from './Actions';

export function getWeather() {
    return function(dispatch) {
        dispatch({type: Actions.REST_QUERIES_START});
        var t0 = performance.now();
        var queriesCount = 0;

        const getWeatherFun = () => {
            fetch("https://api.openweathermap.org/data/2.5/weather?q=Lodz,pl&appid=a4d092435de7aa0a03d195383861eb7f")
                .then(response => response.json())
                .then(resultObj=> {
                    let t1 = performance.now();
                    let timeResult = t1-t0;

                    if(queriesCount++ == 5){
                        dispatch({
                            type: Actions.REST_QUERIES_END,
                            payload: {
                                result: JSON.stringify(resultObj),
                                executionTime: timeResult
                            }
                        });
                    } else {
                        getWeatherFun();
                    }
                });
        }

        setTimeout(function() {
            getWeatherFun();
        }, 1);
    }
}