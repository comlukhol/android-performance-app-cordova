import * as DatabaseUtils from '../common/DatabaseUtils';
import * as Actions from './Actions';

export function readFromDatabase() {
    return function(dispatch) {
        dispatch({type: Actions.READ_FROM_DATABASE_START});

        var t0;
        var db = window.sqlitePlugin.openDatabase({name: 'android-performance-tests.db', location: 'default'});

        DatabaseUtils.recreateSampleEntityTable(db, result => {
            DatabaseUtils.insertAll(db, "sample_entity", result => {
                t0 = performance.now();
                DatabaseUtils.selectAll(db, "sample_entity", rows => {
                    const t1 = performance.now();
                    const timeResult = t1-t0;
                    DatabaseUtils.dropTable(db, "sample_entity", result => {
                        dispatch({
                            type: Actions.READ_FROM_DATABASE_END,
                            payload: {
                                result: rows.length,
                                executionTime: timeResult
                            }
                        });
                    });
                });
            });
        });
    }
};