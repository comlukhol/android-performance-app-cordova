import * as Actions from './Actions';

export function loadLocationAction() {
    return function(dispatch) {
        dispatch({type: Actions.LOAD_LOCATION_START});
        var t0 = performance.now();
        navigator.geolocation.getCurrentPosition(position => {
            let t1 = performance.now();
            let timeResult = t1-t0;

            dispatch({
                type: Actions.LOAD_LOCATION_END,
                payload: {
                    result: "Latitude: " + position.coords.latitude + ", Longitude: " + position.coords.longitude,
                    executionTime: timeResult
                }
            });
        }, error => {
            let t1 = performance.now();
            let timeResult = t1-t0;

            dispatch({
                type: Actions.LOAD_LOCATION_END,
                payload: {
                    result: "Nie można odczytać lokalizacji!",
                    executionTime: timeResult
                }
            });
        }, { enableHighAccuracy: true });
    }
}