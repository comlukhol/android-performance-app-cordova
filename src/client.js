import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import Layout from './components/Layout';

import ArithmeticPage from './pages/ArithmeticPage';
import JsonSerializationPage from './pages/JsonSerializationPage';
import JsonDeserializationPage from './pages/JsonDeserializationPage';
import ReadFromDevicePage from './pages/ReadFromDevicePage';
import SaveToDevicePage from './pages/SaveToDevicePage';
import SaveToDatabasePage from './pages/SaveToDatabasePage';
import ReadFromDatabasePage from './pages/ReadFromDatabasePage';
import LoadLocationPage from './pages/LoadLocationPage';
import RestQueriesPage from './pages/RestQueriesPage';

import * as PermissionManager from './common/PermissionManager';
import * as IntentManager from './common/IntentManager';
import * as TestRest from './common/TestRest';

ReactDOM.render(
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path="/" component={Layout}>
                <IndexRoute component={ArithmeticPage}></IndexRoute>
                <Route path="/arithmeticPage" component={ArithmeticPage} />
                <Route path="/jsonSerializationPage" component={JsonSerializationPage} />
                <Route path="/jsonDeserializationPage" component={JsonDeserializationPage} />
                <Route path="/readFromDevicePage" component={ReadFromDevicePage} />
                <Route path="/saveToDevicePage" component={SaveToDevicePage} />
                <Route path="/saveToDatabasePage" component={SaveToDatabasePage} />
                <Route path="/readFromDatabasePage" component={ReadFromDatabasePage} />
                <Route path="/loadLocationPage" component={LoadLocationPage} />
                <Route path="/restQueriesPage" component={RestQueriesPage} />
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app')
);

document.addEventListener('deviceReady', function() {
    PermissionManager.checkPermission();
    IntentManager.start();
});